;; tabs and tabbar
(setq-default tab-bar-tab-hints t  ;; show tab numbers
	      tab-bar-close-last-tab-choice 'tab-bar-mode-disable
	      tab-bar-show 1)
(provide 'emux-tab)
