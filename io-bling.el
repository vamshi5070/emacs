 (vertico-mode)
	    (setq vertico-count 10
		  vertico-cycle t)

	(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
	(define-key vertico-map (kbd "C-j") #'vertico-next)
	(define-key vertico-map (kbd "C-k") #'vertico-previous)
	;; (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

       (setq completion-styles '(orderless)
	     completion-category-defaults nil
	     completion-category-overrides '((file (styles partial-completion))))

    (global-set-key (kbd "C-x b") 'consult-buffer)
(provide 'io-bling)
