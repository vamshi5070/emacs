(add-to-list 'load-path "~/emacs")

(defun reload ()
 (interactive )
(load-file (expand-file-name "~/.emacs.d/init.el")))

(defun private-config ()
      (interactive )
(find-file (expand-file-name "~/emacs/README.org")))
 ;; (require 'emux-minibuffer)

(global-visual-line-mode t)
  ;; (require 'emux-minibuffer)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(setq package-list
      '(
	;; cape                ; Completion At Point Extensions
	orderless           ; Completion style for matching regexps in any order
	vertico             ; VERTical Interactive COmpletion
	;; marginalia          ; Enrich existing commands with completion annotations
	consult             ; Consulting completing-read
	;; corfu               ; Completion Overlay Region FUnction
	;; deft                ; Quickly browse, filter, and edit plain text notes
	;; elfeed              ; Emacs Atom/RSS feed reader
	;; elfeed-org          ; Configure elfeed with one or more org-mode files
	;; citar               ; Citation-related commands for org, latex, markdown
	;; citeproc            ; A CSL 1.0.2 Citation Processor
	;; flyspell-correct-popup ; Correcting words with flyspell via popup interface
	;; flyspell-popup      ; Correcting words with Flyspell in popup menus
	;; guess-language      ; Robust automatic language detection
	;; helpful             ; A better help buffer
	haskell-mode
	nix-mode
	rust-mode
	;; cider
	clojure-mode
	;; evil
	;; org
	;; org-babel
	org-modern
	ace-window
	corfu
	cape	
	magit
	mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
	envrc   ; envrc
	;; imenu-list          ; Show imenu entries in a separate buffer
	;; magit               ; A Git porcelain inside Emacs.
	;; markdown-mode       ; Major mode for Markdown-formatted text
	;; multi-term          ; Managing multiple terminal buffers in Emacs.
	;; pinentry            ; GnuPG Pinentry server implementation
	;; use-package         ; A configuration macro for simplifying your .emacs
	;; which-key
    ))         ; Display available keybindings in popup

;; Install packages that are not yet installed
(dolist (package package-list)
  (straight-use-package package))

(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (defalias 'yes-or-no-p 'y-or-n-p))

(envrc-global-mode 1)

(prefer-coding-system 'utf-8)	        ;; Encoding
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

(set-face-attribute 'default nil
		      :font "recursive"
				     :height 180
				     :weight 'regular)

     (set-face-attribute 'fixed-pitch nil
			   :font "recursive"
			 :height 220
			  :weight 'regular)

(setq modus-themes-italic-constructs t
	  modus-themes-bold-constructs nil
	  ;; modus-themes-variable-pitch-ui t
	 modus-themes-region '(bg-only no-extend)
	  modus-themes-hl-line '(accented) 
	  modus-themes-syntax '(yellow-comments)
	  modus-themes-mode-line '(accented 3d borderless))
;; Color modeline in active window, remove border
   (setq modus-themes-headings ;; Makes org headings more colorful
	 '((t . (rainbow))))
(load-theme 'modus-operandi t)

(global-hl-line-mode 1)

(require 'dired)

      (put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
 (add-hook 'dired-mode-hook 'dired-hide-details-mode)
 ;; (add-hook 'dired-mode-hook 'dired-omit-mode)
;; (setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
;; 	      dired-recursive-deletes 'top  ;; Always ask recursive delete
;; 	      dired-dwim-target t	    ;; Copy in split mode with p
;; 	      dired-auto-revert-buffer t
;; 	      dired-listing-switches "-alh -agho --group-directories-first"
;; 	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
;; 	      dired-isearch-filenames 'dwim
;; 	      dired-omit-files "^\\.[^.].*"
;; 	      dired-omit-verbose nil
;; 	      dired-hide-details-hide-symlink-targets nil
;; 	      delete-by-moving-to-trash t
;; 	      )
(define-key dired-mode-map (kbd "j") 'dired-next-line)
     (define-key dired-mode-map (kbd "k") 'dired-previous-line)
     (define-key dired-mode-map (kbd "h") 'dired-up-directory)
     (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
      (define-key dired-mode-map (kbd "/") 'dired-goto-file)

(defun emux/horizontal-split ()
   (interactive )
   (split-window-below)
   (other-window 1))

(defun emux/vertical-split ()
   (interactive )
   (split-window-right)
   (other-window 1))

(defun emux/up-window ()
   (interactive )
   (other-window -1))
(defun emux/down-window ()
   (interactive )
   (other-window 1))

;; Isearch

(setq-default search-nonincremental-instead nil    ;; No incremental if enter & empty
	      lazy-highlight-no-delay-length 1     ;; normal delay
	      ;; lazy-highlight-initial-delay 0
	      isearch-allow-scroll t 	           ;; Permit scroll can be 'unlimited
	      isearch-lazy-count t
	      search-ring-max 256
	      regexp-search-ring-max 256
	      isearch-yank-on-move 'shift          ;; Copy text from buffer with meta
	      ;; isearch-wrap-function #'ignore     ;; Look at the emacs-major-version check
	      ;; isearch-wrap-pause t               ;; Disable wrapping nil.
	      isearch-repeat-on-direction-change t ;; Don't go to the other end on direction change
	      isearch-lax-whitespace t
	      isearch-regexp-lax-whitespace t      ;; swiper like fuzzy search
	      search-whitespace-regexp ".*?"
	      ;; Emacs version > 28
	      lazy-highlight-no-delay-length 1     ;; use this instead of lazy-highlight-initial-delay
	      isearch-allow-motion t
	      isearch-forward-thing-at-point '(region symbol sexp word)
	      ;; isearch-motion-changes-direction t
	      )

 (defun my/isearch-exit-other-end ()
    (interactive)
    (when isearch-other-end
      (goto-char isearch-other-end))
    (call-interactively #'isearch-exit))

  (define-key isearch-mode-map (kbd "C-RET") #'my/isearch-exit-other-end)
  (define-key isearch-mode-map (kbd "C-<return>") #'my/isearch-exit-other-end)
  (define-key isearch-mode-map [remap isearch-abort] #'isearch-cancel)
  (define-key isearch-mode-map [remap isearch-delete-char] #'isearch-del-char)
  (define-key search-map "." #'isearch-forward-thing-at-point)
  ;; )

;; Save history
(savehist-mode t)
;; saveplace
(save-place-mode 1)                           ;; Remember point in files
(setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;;__________________________________________________________
;; mode-line
;; (setq-default mode-line-position-column-line-format '(" (%l,%C)")  ;; column number start on 1
;;               ;; mode-line-compact t                                  ;; no spaces on ml
;; 	      mode-line-frame-identification " "                   ;; no F1 in term
;;               mode-line-front-space " "                            ;; no - on the very left
;;               mode-line-end-spaces " "                             ;; no ---- on the right.
;;               mode-line-mule-info ""                               ;; no UUU: on the left.
;;               )

(setq-default mode-line-format 
	      (list  "  " mode-line-modified "  | %m:" " %b |"   " %l ," "%C |"   )

)

(column-number-mode 1)

(global-display-line-numbers-mode 1)
     (setq display-line-numbers-type 'relative)
     (dolist (mode '( org-mode-hook
		       prog-mode-hook
		       ;; term-mode-hook
		       ;; vterm-mode-hook
		       ;; shell-mode-hook
		       ;; dired-mode-hook
		       ;; eshell-mode-hook
		       ))
 (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

;; (require 'io-minibuffer)

;;__________________________________________________________
;; minibuffers

;; These two must be enabled/disabled together
(setq-default ;; enable-recursive-minibuffers t     ;; Enable nesting in minibuffer
	      completion-show-help nil           ;; Don't show help in completion buffer
	      completion-auto-help 'lazy
	      completion-auto-select t
	      completion-wrap-movement t
	      completions-detailed t             ;; show more detailed completions
	      completions-format 'one-column     ;; Vertical completion list
	      completions-max-height 15
	      completion-styles '(substring partial-completion emacs22)
	      ;; M-x show context-local commands
	      read-extended-command-predicate #'command-completion-default-include-p
	      read-file-name-completion-ignore-case t
	      read-buffer-completion-ignore-case t
	      completion-ignore-case t)

;; These two must be enabled/disabled together
;; (setq-default enable-recursive-minibuffers t) ;; Enable nesting in minibuffer
;; (minibuffer-depth-indicate-mode 1)            ;; Mostrar nivel de nesting en minibuffer

;; (setq minibuffer-eldef-shorten-default t)
(add-hook 'minibuffer-setup-hook #'my/unset-gc)
(add-hook 'minibuffer-exit-hook #'my/restore-gc)

;; Arrows up/down search prefix in history like `history-search-backward' in bash
;; (keymap-set minibuffer-local-map "<down>" #'next-complete-history-element)
;; (keymap-set minibuffer-local-map "<up>" #'previous-complete-history-element)

(defun my/completion-setup-hook ()
  "My hook for Completions window."
  (with-current-buffer standard-output
    (setq-local mode-line-format nil)
    (display-line-numbers-mode -1)))

(add-hook 'completion-setup-hook #'my/completion-setup-hook 10)

;; (require 'io-languages)
;; (require 'emux-minibuffer)
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
		(setq  haskell-interactive-popup-errors nil)

;; (require 'emux-org)
  ;; (require 'emux-minibuffer)
  (setq org-ellipsis " ▼ ") ;; ⤵
      (add-hook 'org-mode-hook #'org-shifttab)
  
;; Minimal UI
;;(package-initialize)
;;(menu-bar-mode -1)
;;(tool-bar-mode -1)
;;(scroll-bar-mode -1)
;;(modus-themes-load-operandi)

;; Choose some fonts
;; (set-face-attribute 'default nil :family "Iosevka")
;; (set-face-attribute 'variable-pitch nil :family "Iosevka Aile")
;; (set-face-attribute 'org-modern-symbol nil :family "Iosevka")

;; Add frame borders and window dividers
(modify-all-frames-parameters
 '((right-divider-width . 40)
   (internal-border-width . 40)))
(dolist (face '(window-divider
                window-divider-first-pixel
                window-divider-last-pixel))
  (face-spec-reset-face face)
  (set-face-foreground face (face-attribute 'default :background)))
(set-face-background 'fringe (face-attribute 'default :background))

(setq
 ;; Edit settings
 org-auto-align-tags nil
 org-tags-column 0
 org-catch-invisible-edits 'show-and-error
 org-special-ctrl-a/e t
 org-insert-heading-respect-content t

 ;; Org styling, hide markup etc.
 org-hide-emphasis-markers t
 org-pretty-entities t
 org-ellipsis "…"

 ;; Agenda styling
 org-agenda-block-separator ?─
 org-agenda-time-grid
 '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
 org-agenda-current-time-string
 "⭠ now ─────────────────────────────────────────────────")

(global-org-modern-mode)

(global-set-key (kbd "C-z" ) #'(lambda () (message "pressed C-z")))

(require 'io-cape)
  ;; (require 'emux-minibuffer)

;; (require 'io-bling)
;; (require 'emux-minibuffer)

(require 'io-window)
;; (require 'emux-minibuffer)

;; (require 'ghcid)
;; (require 'emux-minibuffer)

(require 'emux-keybindings)
