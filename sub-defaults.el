(setq-default vc-follow-symlinks t	    ;; Open links not open
	      vc-display-status nil
	      ;;tab-always-indent complete  ;; make tab key do indent only
	      ring-bell-function #'ignore
	      user-full-name "Vamshi"
	      initial-scratch-message (format ";; Welcome %s!!" user-full-name)

	      inhibit-startup-message t
	      inhibit-startup-screen t
	      ;;tab-width 4		    ;; Tabulador a 4
	      ;;indent-tabs-mode t	    ;; Indent with tabs
	      ;;fill-column 80		    ;; default is 70
	      make-backup-files nil	    ;; Sin copias de seguridad
	      create-lockfiles nil	    ;; No lock files, good for tramp
	      visible-bell nil		    ;; Flash the screen (def)
	      confirm-kill-processes nil    ;; no ask kill processes on exit
	      ;; read-key-delay 0.01           ;; already default
	      recenter-redisplay nil
	      ;;recenter-positions '(top middle bottom)
	      ;; line-move-visual nil
	      backward-delete-char-untabify-method nil ;; Don't untabify on backward delete

	      split-width-threshold 140     ;; Limit for vertical split (default 160)
	      ;; kill-whole-line t
	      load-prefer-newer t
	      ;; mark-even-if-inactive nil    ;; no mark no region
	      mark-ring-max 128             ;; Max number of marks in the ring
	      set-mark-command-repeat-pop t ;; Repeat pop mark with C-SPC
	      next-screen-context-lines 5   ;; Lines of continuity when scrolling
	      fast-but-imprecise-scrolling t
	      scroll-error-top-bottom t	    ;; Move cursor before error scroll
	      scroll-preserve-screen-position t	  ;; Cursor keeps screen pos
	      scroll-margin 1		    ;; Margen al borde
	      scroll-step 1		    ;; Scroll step (better conservatively)
	      scroll-conservatively 1000
	      window-combination-resize t   ;; Windows resize proportional
	      x-wait-for-event-timeout nil  ;; Espera por eventos en X
	      jit-lock-stealth-load 60
	      jit-lock-stealth-time 4
	      inhibit-default-init t	    ;; Avoid emacs default init
	      term-suppress-hard-newline t  ;; Text can resize
	      echo-keystrokes 0.01	    ;; Muestra binds in echo area
	      confirm-kill-emacs nil        ;; No confirm exit emacs
	      disabled-command-function nil
	      auto-save-default nil         ;; No autosave
	      auto-save-list-file-name nil
	      ;; minibuffer interaction
	      minibuffer-message-timeout 1  ;; default 2
	      read-quoted-char-radix 16     ;; Read number of chars with C-q
	      ;; kill-buffer-query-functions nil
	      kill-do-not-save-duplicates t   ;; duplicate kill ring entries
	      kill-ring-max (* kill-ring-max 2)   ;; increase kill ring

	      eval-expression-print-length nil
	      eval-expression-print-level nil
	      enable-remote-dir-locals t      ;; Open remote dir locals.
	      suggest-key-bindings t          ;; enable show keybindings in completions

	      truncate-lines t
	      save-interprogram-paste-before-kill t ;; Save clipboard before replace
	      minibuffer-eldef-shorten-default t

	      goto-line-history-local t         ;; Buffer local goto-line history

	      uniquify-buffer-name-style 'post-forward
	      switch-to-buffer-obey-display-actions t ;; switching the buffer respects display actions
	      bookmark-menu-confirm-deletion t    ;; ask confirmation to delete bookmark
	      bookmark-fontify t                  ;; Colorize bookmarked lines with bookmark-face
	      bookmark-save-flag 1                ;; Save bookmarks immediately when added
	      idle-update-delay 0.25              ;; idle to update screen

	      ;; translate-upper-case-key-bindings nil ;; Make keybindings case sensitive
	      outline-minor-mode-use-buttons t      ;; Use buttons to hide/show outlines
	      ;; For when hide-if mode is enabled.
	      hide-ifdef-shadow t
	      hide-ifdef-initially t

	      help-window-select t                  ;; always select help windoes
	      history-delete-duplicates t           ;; delete duplicates in commands history)

	      find-library-include-other-files nil  ;; find-library only shows libraries, not random files.
	      ;; Man
	      Man-notify-method 'pushy              ;; Man open links in same window

	      view-read-only t                      ;; buffers visiting files read-only do so in view mode
	      kill-read-only-ok t                   ;; don’t signal an error for killing read-only text.
	      debugger-stack-frame-as-list t        ;; display call stack frames as lists.
	      async-shell-command-display-buffer nil ;;command buffer wait until there is output

	      register-preview-delay 0.0            ;; for register view remvoe delay.
	      )
